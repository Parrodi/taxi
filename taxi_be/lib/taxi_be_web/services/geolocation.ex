defmodule TaxiBeWeb.Geolocation do
    @geocodingURL "https://api.mapbox.com/geocoding/v5/mapbox.places/"
    @directionsURL "https://api.mapbox.com/directions-matrix/v1/mapbox/driving/"

    @token "pk.eyJ1IjoicGFycm9kaSIsImEiOiJjazFzMGVhMTEwM3RmM21xejd3bGFzMjN5In0.JsrMKWwxRZttK-uEMGMc-g" 


    def geocode(address) do
        case HTTPoison.get(
            @geocodingURL <> URI.encode(address) <>
            ".json?access_token=" <> @token
        ) do
            {:ok, %{body: bodyStr}} ->
                { :ok,
                    bodyStr
                    |> Jason.decode!
                    |> Map.fetch!("features")
                    |> Enum.at(0)
                    |> Map.fetch!("center")
                }
            _ -> {:error, "Something wrong with Mapbox call"}
        end
    end
    

    def distance_and_duration(origin_coord,taxis) do
        myUrl = @directionsURL <> "#{Enum.join(origin_coord, ",")};#{Enum.at(taxis,0).longitude},#{Enum.at(taxis,0).latitude};#{Enum.at(taxis,1).longitude},#{Enum.at(taxis,1).latitude};#{Enum.at(taxis,2).longitude},#{Enum.at(taxis,2).latitude}"<> "?access_token=" <> @token
        IO.inspect(myUrl)
        %{body: body} = HTTPoison.get!(myUrl)
        durations =
            body
            |> Jason.decode!
            |> Map.fetch!("durations")

        {durations}
    end
end
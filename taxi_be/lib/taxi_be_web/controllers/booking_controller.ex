defmodule TaxiBeWeb.BookingController do
    use TaxiBeWeb, :controller
    @geocodingURL "https://api.mapbox.com/geocoding/v5/mapbox.places/"
    @token "pk.eyJ1IjoicGFycm9kaSIsImEiOiJjazFzMGVhMTEwM3RmM21xejd3bGFzMjN5In0.JsrMKWwxRZttK-uEMGMc-g"
    @taxis [
    %{nickname: "angelopolis", latitude: 19.0319783, longitude: -98.2349368},
    %{nickname: "arcangeles", latitude: 19.0061167, longitude: -98.2697737},
    %{nickname: "destino", latitude: 19.0092933, longitude: -98.2473716}
    ]

def create(conn, %{"pickup_address" => pickup_address, "dropoff_address" => dropoff_address}) do
    {:ok, coord1} = TaxiBeWeb.Geolocation.geocode(pickup_address)
    {:ok, coord2} = TaxiBeWeb.Geolocation.geocode(dropoff_address)
    distances = TaxiBeWeb.Geolocation.distance_and_duration(coord1,@taxis)
    IO.inspect(distances)
    value = checkCloser(distances)
    value = Enum.at(@taxis,value).nickname
    json(conn, %{msg: value})
end
def checkCloser(distances)do
    list = Tuple.to_list(distances)
    list = Enum.at(list,0)
    list = List.delete_at(list,0)
    list = Enum.map(list, fn x -> Enum.at(x,0) end)
    value = Enum.min(list)
    value = Enum.find_index(list, fn x -> x==value end)
end
end